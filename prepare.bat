﻿echo off

echo %DELIM_STRING%
echo INFO: prepare ci script
time /t
set CI_PATH=%CI_PROJECT_DIR%\ci

echo ci path is %CI_PATH%

if not exist %CI_PATH% (
    git clone https://gitlab.com/tsysm/cigears.git %CI_PATH%
)

cd %CI_PATH%
REM Здесь можно и нужно указать коммит, ветку или тэг версии конвеера, которая будет использоваться в данном репозитории
git checkout master
git pull

cd %CI_PROJECT_DIR%